= SDP Title

This is an introduction to my assembly.

== Architecture


include::concept-module-template-short.adoc[]

== SOP
include::procedure-module-template.adoc[]

== References
include::reference-module-template.adoc[]

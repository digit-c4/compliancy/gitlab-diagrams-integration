# Content creation process 

This workflow uses basic Gitlab terminology. If you haven't done so, check out the [glossary](gitlab-specifics.md\gitlab-documentation-glossary.md) introducing you to these concepts.

## Workflow

```mermaid
flowchart LR
    A(identify need) --> B(produce/update<br/>doc)
    B --> C(open<br/>merge request)
    C --> D(review)
    D -->|changes required| B
    D -->|OK| F(merge)
```

## Activities

### **Identify need**
Gather requirements for your document. Focus on _what_ the reader wants to achieve through your document and _why_. 
> **Hint**: If you are familiar with user stories you can think of the requirements for your documents in this form:
> 
> As a L2 engineer, I want to understand the system's workflow and architecture, so that I can troubleshoot issues effectively and escalate them appropriately.
> 
>As a L1 engineer, I need clear step-by-step guidance on routine system checks, so that I can perform daily tasks efficiently and with fewer errors

### **Produce or update documen**
In the [designated branch](gitlab-specifics.md\branching-workflow.md), draft the document from scratch based on a [template](document-management-topics\modular-docs\templates) or make a specific change on an existing document. Adhere to any guidelines related to your documents.


> When contributing to a project's documentation, ensure that
> - you create a new branch,
> - you commit your changes in logical and atomic units and [write informative commit messages](document-management-topics\guidelines\commit-message-guidelines.md) that describe the purpose of the changes.

### **Create a merge request**
When your document is ready for review, submit it to a designated reviewer for approval by opening a _merge request_ (MR).

> When opening a merge request, you need to provide the appropriate context for the reviewer to understand what your proposal is about. Check our merge request guidelines [here](document-management-topics\guidelines\merge-request-content-guidelines.md).

### **Review**
The reviewer checks the document and approves it or requires edits. Your merge request remains open while you are making adjustments and exchanging comments with your reviewer.

### **Merge**
The reviewer merges the changes from your branch to the master branch.

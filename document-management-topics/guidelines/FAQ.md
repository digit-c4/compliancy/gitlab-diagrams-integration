## Following stakeholders' input, here are the points to review:

| Question | Answer |
| ------ | ----------- |
| Can I insert my existing diagrams in visio? | Import your Visio files into draw.io using "File → Import from... → Device or URL → Choose file → Import |
| Can I use the constructors stencils in the same way I do in Visio? | Import Visio stencils using "File → New Library → From Device" |
| What about lists, reports and xls files? | GitLab is not meant to be a general-purpose file storage solution, but primarily excels at  versioning text files.  <br> If the purpose is really to maintain historical versions of these excel files, consider using csv format which will display you data as a table. Check out an [example](document-management-topics\tests\KPIs.csv). |
| What about documentation with sensitive content? | For private projects under licence, the access rights are defined by the project owner and maintainers. However, a part of the service documentation is supposed to be non-sensitive exemplary documentation. 
| Are the stats required as KPI metrics available and exportable? | checking |
| What are the types of documents?   | __TO BE UPDATED__ <br> __Techical documentation__ <br>``source code`` ``SDP`` ``architecture diagram`` ``data model`` <br> __Operational documentation__<br> ``SOP`` ``admin guide`` ``PIR`` <br> __User documentation__ <br>``user guide`` <br> __NMS-III standards__ <br> ``policy`` ``framework`` ``process`` ``best-practices``|
| What is the list of required templates? | tbc <br> ``CONTRIBUTING`` ``SDP`` ``SOP`` ``PIR`` ``project creation`` |


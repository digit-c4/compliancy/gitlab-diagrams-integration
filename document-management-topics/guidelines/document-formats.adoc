= Documentation Formats in GitLab

GitLab supports a variety of documentation formats that cater to different use cases.

== Markdown (.md)

Markdown is a lightweight markup language that allows you to format text using plain-text syntax. It's one of the most popular formats for documentation in GitLab repositories.

* Commonly used for READMEs, CONTRIBUTING guides, and other documentation.
* Supported features: headings, lists, links, images, tables, and more.

== AsciiDoc (.adoc)

AsciiDoc is a more powerful and flexible markup language compared to Markdown.

* Suitable for complex documentation projects.
* Offers features like conditional text, inclusion of other documents, and more versatile table structures.

== Others

GitLab also allows for direct rendering of other file formats like:

* CSV (.csv)
* Rich Text Format (.rtf)
* Plain Text (.txt)

== Additional Resources
link:https://docs.gitlab.com/ee/user/markdown.html[GitLab Flavored Markdown (GLFM)]

link:https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet[Markdown cheatsheet]

link:https://docs.gitlab.com/ee/user/asciidoc.html[AsciiDoc]

link:https://docs.gitlab.com/ee/user/project/repository/csv.html[CSV files]

link:https://docs.gitlab.com/ee/user/rich_text_editor.html[Rich text editor]


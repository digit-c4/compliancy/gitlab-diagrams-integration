# **Guidelines for Effective Merge Requests**

When opening a merge request, you need to ensure that you provide the appropriate context for the reviewer to understand what your proposal is about. Please adhere to the following guidelines:

| Guideline | Description | 
| ---      | ---      |
| **Set a descriptive title**   | The title should quickly inform a reviewer of the main purpose of the merge request. E.g., "Revise network configuration section for clarity".
|**Start with a summary**    | Begin with a brief summary of the main changes or additions the merge request brings.      |
| **List major changes**    | If needed, enumerate the main changes or additions in a bullet-point or numbered list format.      |
| **Provide context**      | If the merge request is addressing feedback, fixing a bug, or is part of a bigger task, provide context. This helps reviewers to quickly understand your purpose.      |
| **Link to related issues**  | Always link to any related issues, discussions, or previous merge requests, e.g., "Closes #456" or "Related to #789".       |
| **Request specific feedback**   | If there are areas you're unsure about or want a particular focus on during the review, mention them.  |
| **Update upon changes**   | If you make changes after initial feedback, update the merge request description to reflect what you've done.  |

A merge request looks like this:

![Alt Text](images/merge_request.PNG "merge-request")

# GitLab Documentation Glossary

The table below lists basic terms and concepts commonly used in GitLab, relevant to the documentation process.
> If you are unfamiliar with vesrion cotrol basics, check out the [Git-book](https://git-scm.com/book/en/v2) sections 1, 2 and 3.

| Term | Definition |
| ---- | ---------- |
| **Master or Main Branch** | The default branch that holds the approved and definitive version of your project. This branch is protected and updated only through merge requests. |
| **Branch** | A parallel version of a repository. It allows you to work without affecting the main or master branch. Each branch provides an isolated environment for developing and testing new documents and maintains its own set of commits, representing a unique history of changes specific to that branch. <br> Check out the [relevant workflow](gitlab-specifics.md\how-to-create-a-branch.md). |
| **Commit** | A record of changes to one or more files in the repository. You can think of it as a check-in of your document to register a new version of if. Your commit messages should adhere to [guidelines](document-management-topics\guidelines\commit-message-guidelines.md).|
| **Merge Request (MR)** | A request to merge changes from one branch into another, usually into the main branch of the repository. |
| **Merge** | Merging involves combining the changes from one branch into another. A merge is subject to review.  |
| **Merge Conflict** | A situation that arises when GitLab is unable to automatically reconcile changes in documents from two different commits. This conflict occurs because multiple contributors have edited the same lines or blocks of content, and GitLab cannot determine which version to prioritize without human intervention. |
| **Issue** | A place within GitLab to track ideas, enhancements, tasks, or bugs for work on GitLab. |
| **Repository (Repo)** | Where you store your project's files and where they are version-controlled. |
| **Tag** | A label representing a specific commit, often used to mark release points like v1.0. |
| **README** | A file that provides information about the project, usually the first file you see in a repository. |
| **CONTRIBUTING** | A file that gives instructions on how to contribute to the project. |
| **CHANGELOG** | A curated, chronologically ordered list of notable changes for each version of a project. |
| **License** | A file that contains the terms under which the project's code is made available. |
| **Markdown (MD)** | A lightweight markup language with plain-text formatting syntax for readme files and other documentation. |
| **AsciiDoc (adoc)** | A text document format for writing notes, documentation, articles, books, ebooks, slideshows, web pages, man pages and blogs. AsciiDoc files can be converted to many formats including HTML, PDF, EPUB, and others. |

These definitions provide a quick reference to help you get familiar with GitLab workflows and terminology.

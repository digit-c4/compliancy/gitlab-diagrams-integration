# Contributing to Doclab
 
To ensure a smooth collaboration on documentation management, we have established the following guidelines and instructions for contributors. Please read and adhere to them throughout the contribution process.
 
## Table of Contents
 
- [Code of Conduct](#code-of-conduct)
- [Getting Started](#getting-started)
- [How to Contribute](#how-to-contribute)
- [Documentation Guidelines](#documentation-guidelines)

 
## Code of Conduct
 
We expect all contributors to adhere to NMS-III [Code of Conduct](link-to-code-of-conduct). Please make sure to review it before getting involved in the project.
 
## Getting Started
 
Before you begin contributing, please ensure you have completed the following steps:
 
1. Familiarize yourself with the project's architecture, guidelines, and goals.
2. Reach out to NMS-III Compliance team to seek assistance if needed.
 
## How to Contribute
 
To contribute to the project, please follow these steps:
 
1. Review the project's [issue tracker](link-to-issue-tracker) for open issues or feature requests.
2. If you find an issue or feature, you'd like to work on, comment on it to express your interest and ensure no one else is already assigned to it.
3. Create a new branch for your contribution.
4. Implement the necessary changes or additions.
5. Ensure your code adheres to the project's doc-mgt-best practices [code style] (link-to-code-style).
7. Commit your changes with descriptive commit messages. For more info on meaningful commit messages check our [commit-message-guidelines](doc-mgt-best-practices/commit-message-guidelines.md).
8. Submit a merge request, clearly describing the changes you've made and referencing any related issues. For more info on descriptive merge requests check our [merge-request-content-guidelines](doc-mgt-best-practices/merge-request-content-guidelines.md).
 
 
## Documentation Guidelines
 
- 
-
- 

## License
